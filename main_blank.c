// Completely blank frontend for the game, intended as a template. See main_sdl.c for a better example.

#include <stdio.h>

#if defined(WIN32) || defined(_WIN32) || defined(_WIN32_)
	#include <windows.h>
	#define DELAY(IN) (Sleep(IN))
#endif

#undef main

#include "game.h" // keep in mind, MAIN can still access variables from the game!

void signalPlaySFX(uint8_t signal) {};
void signalPlayMUS(uint8_t signal) {};
void signalDraw(uint8_t signal) {};

void saveGame() {};
void loadGame() {};

//void draw() {};
//void keys() {};

int main() {
	uint8_t running = 1;
	start();
	while (running) {
		step();
		DELAY(16.667f);
	}
	
	return 0;
};