// SDL2 frontend for the game

#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_image.h>

#undef main

#include "game.h" // keep in mind, MAIN can still access variables from the game!

#define EDITMODE 1
// the hackiest debug feature TO DATE

// since dynamic font spacing is kinda cringe, have a lookup table :)
// I tried a bitmasking approach, but all it did was increase file size and most likely invoke more array calls, so this'll do
static const uint8_t fontOffsets_left[58] = {
	1, 3, 2, 4, 2, 4, 3, 0, 3, 1, 6, 5, 1, 4, 4, 5, 4, 6, 2, 4, 4, 6, 2, 2, 6, 4,
	6, 6, 9, 10, 3, 9,
	8, 9, 6, 9, 8, 8, 8, 6, 12, 7, 8, 10, 2, 6, 6, 7, 6, 8, 8, 7, 6, 7, 2, 7, 8, 7
};
// TO-DO: make a script that automates the process of going through all and every font frame and picking out font offsets, in case of (mass) font changes

// strings here, for both ease of access and also to reduce redundancy in program string lists
static const char str_on[] = "ON";
static const char str_off[] = "OFF";
static const char str_exit[] = "Exit";
static const char str_savegamename[] = "WTH_SaveGame.bin";
static const char str_levelname[] = "WTH_EditedLevel.bin";

Mix_Music *music[2];

Mix_Chunk *sfx[6];

SDL_Surface *winsurf;
SDL_Texture *wintext;

SDL_Texture *plrsprites[6];
SDL_Texture *sprites[10];

SDL_Rect dsp_rect; // this is used for the sprite's boundaries
SDL_Rect dspdest_rect; // this is the DESTINATION, aka screen position
SDL_Rect fade_rect; // used for fading in/out

SDL_Point zeropoint = {0,0};

SDL_Renderer *render;
SDL_Window *win;

void drawSprite(SDL_Texture *sprite, int x, int y) {
	dspdest_rect = (SDL_Rect){x, y, 0, 0};
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
};

FILE *DaSave;
FILE *DaLevel;

void drawRepeatingSprite(SDL_Texture *sprite, int x, int y) { // this DOESN'T repeat yet, because I am big Stupid(TM)
	int _x = x;
	if (x == y) {
		dspdest_rect.x = dspdest_rect.y = x;
	} else {
		dspdest_rect.x = x;
		dspdest_rect.y = y;
	}
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	uint8_t limit = 0;
	for (; _x < WIDTH && limit <= 30; limit++) {
		if (_x < (-WIDTH << 2)) {
			_x += dspdest_rect.w;
			continue;
		};
		SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
		_x += dspdest_rect.w;
		dspdest_rect.x = _x;
	};
	_x = x - dspdest_rect.w;
	dspdest_rect.x = _x;
	SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
};

void drawSpriteSheeted(SDL_Texture *sprite, int x, int y, int frame, int x_sprdist, int y_sprdist, uint8_t flipped) { // supports only purely horizontal sheets
	if (x < -x_sprdist || x > x_sprdist + WIDTH) return;
	SDL_Rect destrect = {x, y, x_sprdist, y_sprdist};
	dspdest_rect.x = x_sprdist * frame;
	dspdest_rect.y = 0; // this somehow fixes an issue where the sprite would vertically stretch when moving the camera vertically, but crop off within the sprite's intended bounds. what?
	dspdest_rect.w = x_sprdist,
	dspdest_rect.h = y_sprdist,
	SDL_RenderCopyEx(render, sprite, &dspdest_rect, &destrect, 0, &zeropoint, flipped ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE);
};

void drawTextString(const char *stuff, int x, int y, uint8_t color) {
	// to-do: not this shit.
	// I should get to optimizing this
	switch (color)
	{
		case 1: SDL_SetTextureColorMod(sprites[5], 255, 0, 0); break;/*
		case 2: SDL_SetTextureColorMod(sprites[5], 0, 255, 0); break;
		case 3: SDL_SetTextureColorMod(sprites[5], 0, 0, 255); break;
		case 4: SDL_SetTextureColorMod(sprites[5], 255, 255, 0); break;
		case 5: SDL_SetTextureColorMod(sprites[5], 0, 255, 255); break;
		case 6: SDL_SetTextureColorMod(sprites[5], 255, 0, 255); break;
		case 7: SDL_SetTextureColorMod(sprites[5], 0, 255, 255); break;
		case 8: SDL_SetTextureColorMod(sprites[5], 255, 255, 0); break;
		case 9: SDL_SetTextureColorMod(sprites[5], 255, 255, 255); break;
		case 10: SDL_SetTextureColorMod(sprites[5], 0, 0, 0); break;*/
		// these extra colors ain't used
		default: SDL_SetTextureColorMod(sprites[5], 255, 255, 255); break;
	}
	uint16_t _x = x;
	for (uint8_t i = 0; i < 255; i++) {
		if (stuff[i] == '\0') break; // terminator character? then get outta there before stuff gets nasty
		drawSpriteSheeted(sprites[5], _x, y, stuff[i] - 65, 30, 30, SDL_FLIP_NONE);
		_x += (stuff[i] == 32) ? 30 : 30 - fontOffsets_left[stuff[i] - 65];
	};
};

void signalMisc(uint8_t signal) {
    switch (signal)
    {
        case MISC_PAUSEMUSIC: case MISC_RESUMEMUSIC: case MISC_STOPMUSIC:
            if (!(options[0] & WTHOPTS_DOSONG)) break;
            switch (signal)
            {
                case MISC_PAUSEMUSIC: Mix_PauseMusic(); break;
                case MISC_RESUMEMUSIC: Mix_ResumeMusic(); break;
                case MISC_STOPMUSIC: Mix_HaltMusic(); break;
            }
            break;
    }
};

void drawOptions() {
	drawTextString("Level BG", 20,20, menu.menuselect == 0 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_NOFADE) ? str_off : str_on, 320, 20, menu.menuselect == 0 ? 1 : 0);
	drawTextString("Freecam", 20,52, menu.menuselect == 1 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_DEVCAM) ? str_on : str_off, 320, 52, menu.menuselect == 1 ? 1 : 0);
	drawTextString("Double Res", 20,84, menu.menuselect == 2 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_2XSIZE) ? "YES" : "NO", 320, 84, menu.menuselect == 2 ? 1 : 0);
	drawTextString("Fullscreen", 20,116, menu.menuselect == 3 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_FULLSC) ? str_on : str_off, 320, 116, menu.menuselect == 3 ? 1 : 0);
	drawTextString("Music", 20,148, menu.menuselect == 4 ? 1 : 0);
	drawTextString((options[0] & WTHOPTS_DOSONG) ? str_on : str_off, 320, 148, menu.menuselect == 4 ? 1 : 0);
	
	drawTextString(str_exit, 20,200, menu.menuselect == 5 ? 1 : 0);
};

void signalDraw(uint8_t index) {
	switch (index)
	{
		case DRAW_FADEIN: case DRAW_FADEOUT: case DRAW_HALTFADE:
			if (options[0] & WTHOPTS_NOFADE) break;
			switch (index)
			{
				case DRAW_FADEIN:
					fade = 255;
					fademode = 1;
					SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_BLEND);
					break;
				case DRAW_FADEOUT:
					fade = 0;
					fademode = 2;
					SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_BLEND);
					break;
				case DRAW_HALTFADE:
					fade = 0;
					fademode = 0;
					SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_NONE);
					break;
			};
			break;
		case DRAW_PLRUPARROW:
            drawSprite(sprites[7], plr.x - 20 - cam.x, plr.y - 150 - cam.y);
            break;
		case DRAW_CHECK2XSIZE:
			if (options[0] & WTHOPTS_2XSIZE)
				SDL_SetWindowSize(win, 960, 540);
			else
				SDL_SetWindowSize(win, 480, 270);
			SDL_SetWindowPosition(win, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
			break;
		case DRAW_CHECKFULLSC:
			if (options[0] & WTHOPTS_FULLSC)
				SDL_SetWindowFullscreen(win, SDL_WINDOW_FULLSCREEN_DESKTOP);
			else
				SDL_SetWindowFullscreen(win, 0);
			break;
	};
};

// inline the simple stuff
static inline void signalPlaySFX(uint8_t signal) {
	Mix_PlayChannel(-1, sfx[signal], 0);
};

static inline void signalPlayMUS(uint8_t signal) {
	Mix_PlayMusic(music[signal], -1);
};	// ditto, but for music

void saveGame() {
	DaSave = fopen(str_savegamename,"w");
	fwrite(&options, 1, 2, DaSave);
	fclose(DaSave);
};

void loadGame() {
	if (!(DaSave = fopen(str_savegamename,"r"))) return;
	fread(&options, 1, 1, DaSave);
	fclose(DaSave);
};

void saveLVL() {
	DaLevel = fopen(str_levelname,"w");
	uint8_t lvlheader[1];
	lvlheader[0] = interacts_count;
	int16_t saveinter_pos[2];
	uint16_t saveinter_vars[1];
	uint8_t saveinter_flags[2];
	fwrite(&lvlheader, 1, 1, DaLevel);
	for (uint8_t i = 0; i < interacts_count; i++) {
		saveinter_pos[0] = interacts[i].x;
		saveinter_pos[1] = interacts[i].y;
		saveinter_vars[0] = interacts[i].vars[0];
		saveinter_flags[0] = interacts[i].objID;
		saveinter_flags[1] = interacts[i].flags;
		fwrite(&saveinter_pos, 2, 2, DaLevel);
		fwrite(&saveinter_vars, 1, 2, DaLevel);
		fwrite(&saveinter_flags, 2, 1, DaLevel);
	};
	fclose(DaLevel);
};

void loadLVL() {
	if (!(DaLevel = fopen(str_levelname,"r"))) return;
	uint8_t lvlheader[1];
	int16_t saveinter_pos[2];
	uint16_t saveinter_vars[1];
	uint8_t saveinter_flags[2];
	fread(&lvlheader, 1, 1, DaLevel);
	interacts_count = lvlheader[0];
	for (uint8_t i = 0; i < lvlheader[0]; i++) {
		fread(&saveinter_pos, 2, 2, DaLevel);
		fread(&saveinter_vars, 1, 2, DaLevel);
		fread(&saveinter_flags, 2, 1, DaLevel);
		interacts[i].x = saveinter_pos[0];
		interacts[i].y = saveinter_pos[1];
		interacts[i].vars[0] = saveinter_vars[0];
		interacts[i].objID = saveinter_flags[0];
		interacts[i].flags = saveinter_flags[1] | INTER_ACTIVE;
	};
	fclose(DaLevel);
};

void draw() {
	switch (GAME_STATE)
	{
		case 0:
			//SDL_FillRect(winsurf, NULL, 400);
			switch (menu.menuindex)
			{
				case 0:
					drawSprite(sprites[2], 0, 0);
					break;
				case 1:
					drawOptions();
					break;
			}
			drawTextString("ALPHA", 0, 0, 0);
			
			break;
		case 1: case 4:
			//SDL_FillRect(winsurf, NULL, 400);
			switch (level)
			{
				case 0: case 3:
					drawRepeatingSprite(sprites[0], 0 - cam.x, 0 - cam.y);
					break;
				case 1:
					drawSprite(sprites[3], -cam.x, -cam.y);
					break;
				case 2:
					drawRepeatingSprite(sprites[8], 0 - cam.x, 0 - cam.y);
					break;
			};
			
			drawSpriteSheeted(plrsprites[plr.animindex], (plr.x - 75) - cam.x, (plr.y - 100) - cam.y, plr.animframe, 150, 150, (plr.flags & FLAG_FLIPPED) ? 1 : 0);
			
			for (uint8_t i = 0; i < interacts_count; i++) {
				if (!(interacts[i].flags & INTER_ACTIVE)) continue;
				switch (interacts[i].objID)
				{
					case 255: 
						drawSpriteSheeted(sprites[4], (interacts[i].x - 75) - cam.x, (interacts[i].y - 100) - cam.y, interacts[i].vars[1], 150, 150, (plr.x < interacts[i].x) ? 1 : 0);
						break;
					case INTERTYPE_COAT:
						drawSpriteSheeted(sprites[6], interacts[i].x - 35 - cam.x, (interacts[i].y - 100) - cam.y, (plr.flags & FLAG_HASCOAT) ? 1 : 0, 70, 100, 0);
						break;
					case INTERTYPE_DECOR:
						drawSprite(sprites[interacts[i].vars[0]], interacts[i].x - cam.x, interacts[i].y - cam.y);
						break;
					case INTERTYPE_ARTIFACT:
						drawSpriteSheeted(sprites[9], (interacts[i].x - 32) - cam.x, (interacts[i].y - 32) - cam.y, interacts[i].vars[0], 64, 64, 0);
						break;
					default:
						if (GAME_STATE == 4) {
							drawSpriteSheeted(plrsprites[2], (interacts[i].x - 75) - cam.x, (interacts[i].y - 75) - cam.y, 2, 150, 150, 0);
						};
						break;
				}
			};
			switch (edit.objID)
			{
				case 255: 
					drawSpriteSheeted(sprites[4], mx / (win_width / WIDTH), my / (win_height / HEIGHT), 0, 150, 150, 0);
					break;
				case INTERTYPE_COAT:
					drawSpriteSheeted(sprites[6], mx / (win_width / WIDTH), my / (win_height / HEIGHT), 0, 70, 100, 0);
					break;
				case INTERTYPE_ARTIFACT:
					drawSpriteSheeted(sprites[9], mx / (win_width / WIDTH) - 32, my / (win_height / HEIGHT), 0, 64, 64, 0);
					break;
				default:
					drawSpriteSheeted(plrsprites[2], mx / (win_width / WIDTH) - 75, my / (win_height / HEIGHT) - 75, 2, 150, 150, 0);
					break;
			}
			if (fade != 0) {
				SDL_SetRenderDrawColor(render, 0,0,0, fade);
				SDL_RenderFillRect(render, NULL);
			};
			if (GAME_STATE == 1) {
				switch (level)
				{
					case 0: drawTextString("Test Room", 0, 0, 0); break;
					case 1: drawTextString("House", 0, 0, 0); break;
					case 2: drawTextString("Outside of House", 0, 0, 0); break;
					case 3: drawTextString("Endless Sidewalk", 0, 0, 0); break;
					default: drawTextString("Unknown Room", 0, 0, 0); break;
				};
			} else {
				switch (edit.objID)
				{
					case 0: drawTextString("Room Change Trigger", 0, 0, 0); break;
					case 1: drawTextString("Doorway", 0, 0, 0); break;
					case 2: drawTextString("Artifact", 0, 0, 0); break;
					case 3: drawTextString("Movement Tracker", 0, 0, 0); break;
					case 4: drawTextString("Coat", 0, 0, 0); break;
					case 255: drawTextString("You...?", 0, 0, 0); break;
					default: drawTextString("Some Object IDK", 0, 0, 0); break;
				};
				switch (interacts[edit.selIndex].objID)
				{
					case 0: drawTextString("Room Change Trigger", 0, 248, 0); break;
					case 1: drawTextString("Doorway", 0, 248, 0); break;
					case 2: drawTextString("Artifact", 0, 248, 0); break;
					case 3: drawTextString("Movement Tracker", 0, 248, 0); break;
					case 4: drawTextString("Coat", 0, 248, 0); break;
					case 255: drawTextString("You...?", 0, 248, 0); break;
					default: drawTextString("Some Object IDK", 0, 248, 0); break;
				};
			};
			break;
		case 3:
			//SDL_FillRect(winsurf, NULL, 400);
			switch (menu.menuindex)
			{
				case 0:
					drawTextString("MENU",200, 75, 0);
					drawTextString("Play Mode", 200,120, menu.menuselect == 0 ? 1 : 0);
					drawTextString("Edit Mode", 200,152, menu.menuselect == 1 ? 1 : 0);
					drawTextString(str_exit, 200,184, menu.menuselect == 2 ? 1 : 0);
					drawTextString("Save Level", 200,216, menu.menuselect == 3 ? 1 : 0);
					drawTextString("Load Level", 200,248, menu.menuselect == 4 ? 1 : 0);
					break;
				case 1:
					drawOptions();
					break;
			}
			break;
	};
};

uint8_t *keystates;

void keys() {
	if (keystates[SDL_SCANCODE_LEFT]) input_keys |= KEY_LEFT;
	if (keystates[SDL_SCANCODE_RIGHT]) input_keys |= KEY_RIGHT;
	if (keystates[SDL_SCANCODE_UP]) input_keys |= KEY_UP;
	if (keystates[SDL_SCANCODE_DOWN]) input_keys |= KEY_DOWN;
	if (keystates[SDL_SCANCODE_Z]) input_keys |= KEY_JUMP;
	if (keystates[SDL_SCANCODE_V]) input_keys |= KEY_CLICK;
	if (keystates[SDL_SCANCODE_ESCAPE]) input_keys |= KEY_MENU;
	
	if (keystates[SDL_SCANCODE_A]) xtrakeys |= KEY_LEFT;
	if (keystates[SDL_SCANCODE_D]) xtrakeys |= KEY_RIGHT;
	if (keystates[SDL_SCANCODE_W]) xtrakeys |= KEY_UP;
	if (keystates[SDL_SCANCODE_S]) xtrakeys |= KEY_DOWN;
	if (keystates[SDL_SCANCODE_G]) xtrakeys |= 1 << 4;
	if (keystates[SDL_SCANCODE_H]) xtrakeys |= 1 << 5;
	if (keystates[SDL_SCANCODE_J]) xtrakeys |= 1 << 6;
	if (keystates[SDL_SCANCODE_K]) xtrakeys |= 1 << 7;
};

int main() {
	if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) return 1;
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 1, 2048) != 0)
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_WARNING, "No audio!", "The game couldn't find a usable audio system. Check your audio devices!", NULL);
	
	win = SDL_CreateWindow(WINTITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	render = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	
	SDL_RenderSetLogicalSize(render, 480, 270); // keep that cwispy 270P 16:9 wesowutiown UwU
	
	SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_NONE);
	
	winsurf = SDL_GetWindowSurface(win);
	
	music[0] = Mix_LoadMUS("SHOPKEEP.it"); 
	music[1] = Mix_LoadMUS("CRIMSON.it");

	SDL_SetRenderDrawColor(render, 0, 0x01, 0x90, 255);
	keystates = SDL_GetKeyboardState(NULL); // this was called every time keys() is ran, but apparently it's only needed to be called ONCE :)
	
	sfx[0] = Mix_LoadWAV("sfx_step.wav");
	sfx[1] = Mix_LoadWAV("sfx_jump.wav");
	sfx[2] = Mix_LoadWAV("sfx_slam.wav");
	sfx[3] = Mix_LoadWAV("sfx_coatpickup.wav");
	sfx[4] = Mix_LoadWAV("sfx_dooropen.wav");
	sfx[5] = Mix_LoadWAV("sfx_doorclose.wav");
	
	plrsprites[0] = IMG_LoadTexture(render, "sprites/plr_idle.png");
	plrsprites[1] = IMG_LoadTexture(render, "sprites/plr_walk.png");
	plrsprites[2] = IMG_LoadTexture(render, "sprites/plr_idle1.png");
	plrsprites[3] = IMG_LoadTexture(render, "sprites/plr_enterdoor.png");
	plrsprites[4] = IMG_LoadTexture(render, "sprites/plr_asleep.png");
	plrsprites[5] = IMG_LoadTexture(render, "sprites/plr_idle_coatless.png");
	sprites[0] = IMG_LoadTexture(render, "sprites/bg.png");
	sprites[1] = IMG_LoadTexture(render, "sprites/PLAY.png");
	sprites[2] = IMG_LoadTexture(render, "sprites/MENU.png");
	sprites[3] = IMG_LoadTexture(render, "sprites/bg_house.png");
	sprites[4] = IMG_LoadTexture(render, "sprites/you_idle.png");
	sprites[5] = IMG_LoadTexture(render, "sprites/texts/fontmap.png");
	sprites[6] = IMG_LoadTexture(render, "sprites/coatrack.png");
	sprites[7] = IMG_LoadTexture(render, "sprites/uparrow.png");
	sprites[8] = IMG_LoadTexture(render, "sprites/bg_outdoors.png");
	sprites[9] = IMG_LoadTexture(render, "sprites/artifacts.png");
	
	uint8_t running = 1;
	SDL_Event event;
	start();
	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_QUIT: running = 0; break;
			}
		}
		SDL_GetMouseState(&mx, &my);
		SDL_GetWindowSize(win, &win_width, &win_height);
		keys();
		if (fade < 255) draw();
		step();
		
		//wintext = SDL_CreateTextureFromSurface(render, winsurf);
		//SDL_RenderCopy(render, wintext, NULL, NULL);
		//SDL_UpdateWindowSurface(win);
		SDL_RenderPresent(render);
		//SDL_DestroyTexture(wintext);
		SDL_RenderClear(render);
		SDL_Delay(16.667f);
	}
	
	//for (int i = 0; i < 1; i++) {
	//	Mix_FreeMusic(music[i]);
	//}
	
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(win);
	SDL_Quit();
	
	return 0;
};
