# Wake to Hell

A game about a cat who seems to be dealing with some strong issues. To say the least.

It's a free software game (CC0 code and graphics, other mixed but FOSS licenses) that puts you into the shoes of Lukifer Dredd, who must navigate his way through his day as a sufferer of chronic stress. Sounds boring? Probably is.

Made with love and hatred by blitzdoughnuts, author of Ludicrital and professional lunatic.

## Why This Game is Special (ft. Anarch)

- All free assets! Code and original game assets are CC0 (public domain), see licenses for extra details
- Completely non-commerical! No advertising or marketing to be seen here
- Game code is independent from the platforms, allowing for relatively easy porting! Current platforms include:
    - Windows (SDL2, Raylib)
    - Linux (SDL2)
- Compilation is clean! Just a C compiler, a handful of libraries, and optionally one of the build scripts are needed
- Uses ONLY integer math, to please you hardcore suckless *fascists* and/or piss off actually sane programmers! :D
- Low hardware demands! Game logic and (hopefully) platform frontends aren't too costly.
- Made with free software! Notepad++ for code editing, LibreSprite for drawing, and the GNU and Tiny C compilers for Linux and Windows respectively
- Contains philosophical questioning and potential psychological horror. *Yippee, I guess.*
- Has cats!
- Kept simple, stupid! Wake to Hell knows two things: be simple, and be respectful. File I/O is preferred and HIGHLY recommended, but not at all needed, it's configurable in the save file that can be edited with a premade tool, and it doesn't care **at ALL** about how and where it's played; just enjoy the damn game

Much kudos to [Anarch](https://codeberg.org/drummyfish/Anarch/) by drummyfish for this section's inspiration.

## What the Hell is this?

I made this game for a good reason: not only to exercise my storytelling capability and also to have an excuse to make a game entirely in C and (mainly) SDL2, but also to get out a message.

Honestly, it saddens me to see the current world where, on and outside of the Internet, there's a ton of stress and hecticity surrounding everything nowadays. Especially for people with aspergers, autism, or any form of neurodiversity or mental disorder, it's a double edged sword, even if you DON'T account for the shitfest that is the Internet.

And so I kind of decided to vent this over to a tangible interactive medium, to hopefully make more people at least aware of the potential struggles that people in these times go through in social and personal terms.

In short:

I'm crazy and I'm sad, so I made a game to say that.

## Known Bugs, Issues, and Mishaps

As Wake to Hell is a game under heavy development, it's bound to have little bugs and bits that are in dozens of places. Here's some things I've found myself:

# Bugs
- Pausing during the door-opening sequence causes the player sprites to mess up. Maybe it's the player sprite flags?
- Exiting to main menu doesn't reset some game variables. This may cause problems.
- The function for drawing text, by a chance, MIGHT be able to **cause some sort of overflow** if the input string is over 255 characters long. *Like they'd be that long, anyway.*
-- Might be fixed? It now halts when a return character's encountered, but... I dunno.
- RayLib is radically behind the SDL2 frontend, which may cause issues
- The keyboard fetching function on SDL2 always throws a warning due to the NULL given as a parameter for storing... something.
- Tiled sprite rendering doesn't account for going BACKWARDS much.
- Some functions, e.g. sheeted sprite rendering, may have redundant parameters.

# Mishaps
- Player sprites don't have coatless versions
- Coat racks can take your coat from you if you have one. Might be usable as a proper feature?
- Some inputs don't check for the last frame's inputs and work as "is held" rather than "is pressed", e.g. Jump
- Going back to the main menu from the game cauases the background to be black.
- While jumping, you preserve your horizontal momentun. Probably isn't too bad.

## Ideas for What You Can Do
Another part inspired by Anarch.

- Play it, edit it, break it
- Record it, review it
- Add demo files / replay support
- Add a level format and level editor
- Add proper support for different resolutions
- Use the game's engine for your own projects
- Port to your favorite platform
- Put Wake to Hell into Anarch (or vice versa)

## Licensing

The code and graphics *assets* are licensed under CC0.
Ludicrital is a webcomic by blitzdoughnuts, under the CC-BY license.

For extra details, see LICENSES.txt

This README has no specific license, but all I request is, just don't cause chaos with it, okay? You can use it all you want, you can even monetize it, you don't need to credit me, but just don't stir up trouble. I beg.