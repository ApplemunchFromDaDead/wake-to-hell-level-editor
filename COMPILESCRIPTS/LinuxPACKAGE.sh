#!/bin/sh

cd ..

rm ./packaging/WakeToHell.deb
cp WakeToHell ./packaging/usr/bin
cp WTH.s3m ./packaging/usr/bin
cp WTH.mp3 ./packaging/usr/bin
cp sfx_slam.wav ./packaging/usr/bin
cp sfx_jump.wav ./packaging/usr/bin
cp sfx_slam.wav ./packaging/usr/bin
cp -r ./sprites ./packaging/usr/bin
cd ./packaging
dpkg-deb -b . WakeToHell.deb
