#!/bin/sh
# You can change the compiler; I won't gaurantee it'll work, though.

# Buidling for SDL2 requires libsdl2-dev, libsdl2-mixer-dev, and libsdl2-image-dev packages

COMPILER="g++"

clear

echo "Compiling game for SDL2, please wait..."

cd ..

if ${COMPILER} main_sdl.c -lSDL2 -lSDL2_mixer -lSDL2_image -o WakeToHell -fpermissive -no-pie; then
    echo "Compilation succeeded... hopefully."
else
    echo "Compilation failed! Error code: ${$?}"
fi
